﻿namespace Pomodoro
{
    partial class Pomodoro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pomodoro));
            this.tbPomodoro = new System.Windows.Forms.TrackBar();
            this.lblPomodoro = new System.Windows.Forms.Label();
            this.lblPausaCurta = new System.Windows.Forms.Label();
            this.tbPausaCurta = new System.Windows.Forms.TrackBar();
            this.lblPausaLonga = new System.Windows.Forms.Label();
            this.tbPausaLonga = new System.Windows.Forms.TrackBar();
            this.lblPomodoros = new System.Windows.Forms.Label();
            this.tbPomodoros = new System.Windows.Forms.TrackBar();
            this.lblTimer = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.rbPomodoro = new System.Windows.Forms.RadioButton();
            this.rbPausaCurta = new System.Windows.Forms.RadioButton();
            this.rbPausaLonga = new System.Windows.Forms.RadioButton();
            this.lblCPomodoro = new System.Windows.Forms.Label();
            this.lblCPausaCurta = new System.Windows.Forms.Label();
            this.chbSons = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.tbPomodoro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPausaCurta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPausaLonga)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPomodoros)).BeginInit();
            this.SuspendLayout();
            // 
            // tbPomodoro
            // 
            this.tbPomodoro.Location = new System.Drawing.Point(274, 9);
            this.tbPomodoro.Maximum = 25;
            this.tbPomodoro.Name = "tbPomodoro";
            this.tbPomodoro.Size = new System.Drawing.Size(162, 45);
            this.tbPomodoro.SmallChange = 5;
            this.tbPomodoro.TabIndex = 0;
            this.tbPomodoro.TickFrequency = 5;
            this.tbPomodoro.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // lblPomodoro
            // 
            this.lblPomodoro.AutoSize = true;
            this.lblPomodoro.Location = new System.Drawing.Point(442, 9);
            this.lblPomodoro.Name = "lblPomodoro";
            this.lblPomodoro.Size = new System.Drawing.Size(13, 13);
            this.lblPomodoro.TabIndex = 1;
            this.lblPomodoro.Text = "0";
            // 
            // lblPausaCurta
            // 
            this.lblPausaCurta.AutoSize = true;
            this.lblPausaCurta.Location = new System.Drawing.Point(442, 60);
            this.lblPausaCurta.Name = "lblPausaCurta";
            this.lblPausaCurta.Size = new System.Drawing.Size(13, 13);
            this.lblPausaCurta.TabIndex = 3;
            this.lblPausaCurta.Text = "0";
            // 
            // tbPausaCurta
            // 
            this.tbPausaCurta.LargeChange = 1;
            this.tbPausaCurta.Location = new System.Drawing.Point(274, 60);
            this.tbPausaCurta.Maximum = 5;
            this.tbPausaCurta.Name = "tbPausaCurta";
            this.tbPausaCurta.Size = new System.Drawing.Size(162, 45);
            this.tbPausaCurta.TabIndex = 2;
            this.tbPausaCurta.Scroll += new System.EventHandler(this.tbPausaCurta_Scroll);
            // 
            // lblPausaLonga
            // 
            this.lblPausaLonga.AutoSize = true;
            this.lblPausaLonga.Location = new System.Drawing.Point(442, 111);
            this.lblPausaLonga.Name = "lblPausaLonga";
            this.lblPausaLonga.Size = new System.Drawing.Size(13, 13);
            this.lblPausaLonga.TabIndex = 5;
            this.lblPausaLonga.Text = "0";
            // 
            // tbPausaLonga
            // 
            this.tbPausaLonga.Location = new System.Drawing.Point(274, 111);
            this.tbPausaLonga.Maximum = 30;
            this.tbPausaLonga.Name = "tbPausaLonga";
            this.tbPausaLonga.Size = new System.Drawing.Size(162, 45);
            this.tbPausaLonga.SmallChange = 5;
            this.tbPausaLonga.TabIndex = 4;
            this.tbPausaLonga.TickFrequency = 5;
            this.tbPausaLonga.Scroll += new System.EventHandler(this.tbPausaLonga_Scroll);
            // 
            // lblPomodoros
            // 
            this.lblPomodoros.AutoSize = true;
            this.lblPomodoros.Location = new System.Drawing.Point(442, 162);
            this.lblPomodoros.Name = "lblPomodoros";
            this.lblPomodoros.Size = new System.Drawing.Size(13, 13);
            this.lblPomodoros.TabIndex = 7;
            this.lblPomodoros.Text = "0";
            // 
            // tbPomodoros
            // 
            this.tbPomodoros.LargeChange = 1;
            this.tbPomodoros.Location = new System.Drawing.Point(274, 162);
            this.tbPomodoros.Name = "tbPomodoros";
            this.tbPomodoros.Size = new System.Drawing.Size(162, 45);
            this.tbPomodoros.TabIndex = 6;
            this.tbPomodoros.Scroll += new System.EventHandler(this.tbPomodoros_Scroll);
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimer.Location = new System.Drawing.Point(12, 9);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(103, 40);
            this.lblTimer.TabIndex = 8;
            this.lblTimer.Text = "00:00";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 184);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Começar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(93, 184);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Parar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // rbPomodoro
            // 
            this.rbPomodoro.AutoSize = true;
            this.rbPomodoro.Location = new System.Drawing.Point(195, 9);
            this.rbPomodoro.Name = "rbPomodoro";
            this.rbPomodoro.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.rbPomodoro.Size = new System.Drawing.Size(73, 17);
            this.rbPomodoro.TabIndex = 11;
            this.rbPomodoro.TabStop = true;
            this.rbPomodoro.Text = "Pomodoro";
            this.rbPomodoro.UseVisualStyleBackColor = true;
            this.rbPomodoro.CheckedChanged += new System.EventHandler(this.radios_Changed);
            // 
            // rbPausaCurta
            // 
            this.rbPausaCurta.AutoSize = true;
            this.rbPausaCurta.Location = new System.Drawing.Point(185, 60);
            this.rbPausaCurta.Name = "rbPausaCurta";
            this.rbPausaCurta.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.rbPausaCurta.Size = new System.Drawing.Size(83, 17);
            this.rbPausaCurta.TabIndex = 12;
            this.rbPausaCurta.TabStop = true;
            this.rbPausaCurta.Text = "Pausa Curta";
            this.rbPausaCurta.UseVisualStyleBackColor = true;
            this.rbPausaCurta.CheckedChanged += new System.EventHandler(this.radios_Changed);
            // 
            // rbPausaLonga
            // 
            this.rbPausaLonga.AutoSize = true;
            this.rbPausaLonga.Location = new System.Drawing.Point(180, 111);
            this.rbPausaLonga.Name = "rbPausaLonga";
            this.rbPausaLonga.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.rbPausaLonga.Size = new System.Drawing.Size(88, 17);
            this.rbPausaLonga.TabIndex = 13;
            this.rbPausaLonga.TabStop = true;
            this.rbPausaLonga.Text = "Pausa Longa";
            this.rbPausaLonga.UseVisualStyleBackColor = true;
            this.rbPausaLonga.CheckedChanged += new System.EventHandler(this.radios_Changed);
            // 
            // lblCPomodoro
            // 
            this.lblCPomodoro.AutoSize = true;
            this.lblCPomodoro.Location = new System.Drawing.Point(9, 64);
            this.lblCPomodoro.Name = "lblCPomodoro";
            this.lblCPomodoro.Size = new System.Drawing.Size(64, 13);
            this.lblCPomodoro.TabIndex = 14;
            this.lblCPomodoro.Text = "0 Pomodoro";
            // 
            // lblCPausaCurta
            // 
            this.lblCPausaCurta.AutoSize = true;
            this.lblCPausaCurta.Location = new System.Drawing.Point(9, 92);
            this.lblCPausaCurta.Name = "lblCPausaCurta";
            this.lblCPausaCurta.Size = new System.Drawing.Size(74, 13);
            this.lblCPausaCurta.TabIndex = 15;
            this.lblCPausaCurta.Text = "0 Pausa Curta";
            // 
            // chbSons
            // 
            this.chbSons.AutoSize = true;
            this.chbSons.Checked = true;
            this.chbSons.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbSons.Location = new System.Drawing.Point(12, 112);
            this.chbSons.Name = "chbSons";
            this.chbSons.Size = new System.Drawing.Size(56, 17);
            this.chbSons.TabIndex = 16;
            this.chbSons.Text = "Sons?";
            this.chbSons.UseVisualStyleBackColor = true;
            // 
            // Pomodoro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 215);
            this.Controls.Add(this.chbSons);
            this.Controls.Add(this.lblCPausaCurta);
            this.Controls.Add(this.lblCPomodoro);
            this.Controls.Add(this.rbPausaLonga);
            this.Controls.Add(this.rbPausaCurta);
            this.Controls.Add(this.rbPomodoro);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.lblPomodoros);
            this.Controls.Add(this.tbPomodoros);
            this.Controls.Add(this.lblPausaLonga);
            this.Controls.Add(this.tbPausaLonga);
            this.Controls.Add(this.lblPausaCurta);
            this.Controls.Add(this.tbPausaCurta);
            this.Controls.Add(this.lblPomodoro);
            this.Controls.Add(this.tbPomodoro);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Pomodoro";
            this.Text = "Pomodoro";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbPomodoro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPausaCurta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPausaLonga)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPomodoros)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar tbPomodoro;
        private System.Windows.Forms.Label lblPomodoro;
        private System.Windows.Forms.Label lblPausaCurta;
        private System.Windows.Forms.TrackBar tbPausaCurta;
        private System.Windows.Forms.Label lblPausaLonga;
        private System.Windows.Forms.TrackBar tbPausaLonga;
        private System.Windows.Forms.Label lblPomodoros;
        private System.Windows.Forms.TrackBar tbPomodoros;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RadioButton rbPomodoro;
        private System.Windows.Forms.RadioButton rbPausaCurta;
        private System.Windows.Forms.RadioButton rbPausaLonga;
        private System.Windows.Forms.Label lblCPomodoro;
        private System.Windows.Forms.Label lblCPausaCurta;
        private System.Windows.Forms.CheckBox chbSons;

    }
}

