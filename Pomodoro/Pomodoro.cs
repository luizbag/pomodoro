﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.Configuration;

namespace Pomodoro
{
    enum TipoContador
    {
        Pomodoro,
        PausaCurta,
        PausaLonga
    }

    public partial class Pomodoro : Form
    {
        const string POMODORO = "pomodoro";
        const string PC = "pausa_curta";
        const string PL = "pausa_longa";
        const string N_POMODORO = "n_pomodoro";

        int minutos = 0;
        int segundos = 0;
        int pomodoros = 0;
        int pausascurtas = 0;
        int pl = 0;
        List<RadioButton> radios = new List<RadioButton>();
        Dictionary<string, TipoContador> dic = new Dictionary<string, TipoContador>();
        TipoContador tipoContador = TipoContador.Pomodoro;
        SoundPlayer player = new SoundPlayer();

        const string alarmeUrl = "alarm-clock-01.wav";
        const string tickUrl = "clock-ticking-2.wav";

        public Pomodoro()
        {
            InitializeComponent();
            radios.Add(rbPomodoro);
            radios.Add(rbPausaCurta);
            radios.Add(rbPausaLonga);
            dic.Add("rbPomodoro", TipoContador.Pomodoro);
            dic.Add("rbPausaCurta", TipoContador.PausaCurta);
            dic.Add("rbPausaLonga", TipoContador.PausaLonga);
            rbPomodoro.Checked = true;
            carregarConfig();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            lblPomodoro.Text = tbPomodoro.Value.ToString();
        }

        private void preencheTimer(bool contando)
        {
            if (!contando)
            {
                switch (tipoContador)
                {
                    case TipoContador.Pomodoro: minutos = tbPomodoro.Value; break;
                    case TipoContador.PausaCurta: minutos = tbPausaCurta.Value; break;
                    case TipoContador.PausaLonga: minutos = tbPausaLonga.Value; break;
                }
                segundos = 0;
            }
            lblTimer.Text = minutos.ToString().PadLeft(2, '0') + ":" + segundos.ToString().PadLeft(2, '0');
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lblPomodoro.Text = tbPomodoro.Value.ToString();
            lblPausaCurta.Text = tbPausaCurta.Value.ToString();
            lblPausaLonga.Text = tbPausaLonga.Value.ToString();
            lblPomodoros.Text = tbPomodoros.Value.ToString();
            preencheTimer(false);
        }

        private void tbPausaCurta_Scroll(object sender, EventArgs e)
        {
            lblPausaCurta.Text = tbPausaCurta.Value.ToString();
        }

        private void tbPausaLonga_Scroll(object sender, EventArgs e)
        {
            lblPausaLonga.Text = tbPausaLonga.Value.ToString();
        }

        private void tbPomodoros_Scroll(object sender, EventArgs e)
        {
            lblPomodoros.Text = tbPomodoros.Value.ToString();
            pl = tbPomodoros.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            salvarConfig();
            switch (tipoContador)
            {
                case TipoContador.Pomodoro:
                    if (pomodoros != pl && pomodoros <= pausascurtas)
                        comecar();
                    else
                        if (pomodoros == pl)
                            MessageBox.Show("Você merece uma pausa longa!");
                        else
                            MessageBox.Show("Você merece uma pausa curta");
                    break;
                case TipoContador.PausaCurta:
                    if (pomodoros > pausascurtas && pomodoros != pl)
                        comecar();
                    else
                        if (pomodoros == pl)
                            MessageBox.Show("Você merece uma pausa longa!");
                        else
                            MessageBox.Show("Você ainda não tem direito a uma pausa curta, complete 1 pomodoro!");
                    break;
                case TipoContador.PausaLonga:
                    if (pomodoros >= pl)
                        comecar();
                    else
                        MessageBox.Show("Você ainda não tem direito a uma pausa longa, complete " + (pl - pomodoros).ToString() + " pomodoros!");
                    break;
            }
        }

        private void carregarConfig()
        {
            tbPomodoro.Value = Convert.ToInt32(ConfigurationManager.AppSettings[POMODORO]);
            tbPausaCurta.Value = Convert.ToInt32(ConfigurationManager.AppSettings[PC]);
            tbPausaLonga.Value = Convert.ToInt32(ConfigurationManager.AppSettings[PL]);
            tbPomodoros.Value = Convert.ToInt32(ConfigurationManager.AppSettings[N_POMODORO]);
        }

        private void salvarConfig()
        {
            ConfigurationManager.AppSettings[POMODORO] = tbPomodoros.Value.ToString();
            ConfigurationManager.AppSettings[N_POMODORO] = tbPomodoros.Value.ToString();
            ConfigurationManager.AppSettings[PC] = tbPausaCurta.Value.ToString();
            ConfigurationManager.AppSettings[PL] = tbPausaLonga.Value.ToString();
        }

        private void comecar()
        {
            preencheTimer(false);
            if (minutos > 0)
            {
                enableControles(false);
                timer.Interval = 1000;
                timer.Start();
                if (chbSons.Checked)
                {
                    player.SoundLocation = tickUrl;
                    player.PlayLooping();
                }
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            segundos--;
            if (segundos < 0)
            {
                minutos--;
                segundos = 59;
            }
            if (minutos < 0)
            {
                timer.Stop();
                player.Stop();
                if (chbSons.Checked)
                {
                    player.SoundLocation = alarmeUrl;
                    player.Play();
                }
                if (tipoContador == TipoContador.Pomodoro)
                    pomodoros++;
                if (tipoContador == TipoContador.PausaCurta)
                    pausascurtas++;
                if (tipoContador == TipoContador.PausaLonga)
                    pomodoros = pausascurtas = 0;
                enableControles(true);
                preencheTimer(false);
                preencheContadores();
            }
            else
            {
                preencheTimer(true);
            }
        }

        private void preencheContadores()
        {
            lblCPomodoro.Text = pomodoros > 1 ? pomodoros.ToString() + " Pomodoros" : pomodoros.ToString() + " Pomodoro";
            lblCPausaCurta.Text = pomodoros > 1 ? pausascurtas.ToString() + " Pausas Curtas" : pausascurtas.ToString() + " Pausa Curta";
        }

        private void enableControles(bool enable)
        {
            tbPomodoro.Enabled = enable;
            tbPomodoros.Enabled = enable;
            tbPausaLonga.Enabled = enable;
            tbPausaCurta.Enabled = enable;
            rbPomodoro.Enabled = enable;
            rbPausaCurta.Enabled = enable;
            rbPausaLonga.Enabled = enable;
            chbSons.Enabled = enable;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer.Stop();
            player.Stop();
            enableControles(true);
            preencheTimer(false);
        }

        private void radios_Changed(object sender, EventArgs e)
        {
            foreach (var radio in radios)
            {
                if (radio.Checked)
                {
                    tipoContador = dic[radio.Name];
                    preencheTimer(false);
                }
            }
        }
    }
}
